# OpenML dataset: Oilst_Customers_Dataset

https://www.openml.org/d/46105

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The `olist_customers_dataset.csv` offers a comprehensive snapshot of customer details from the Olist e-commerce platform. This dataset encapsulates essential customer information, providing a foundation for understanding consumer demographics, regional distribution, and identifying unique shoppers on the platform.

Attribute Description:
- `customer_id` (String): A hexadecimal ID unique to each order placed on the Olist platform, serving as a primary key for order tracking. Examples: '827c1daa...', 'a3d6e70f...'.
- `customer_unique_id` (String): A distinct identifier for each customer, irrespective of the number of orders they place. This ID helps in distinguishing individual consumers and analyzing their purchasing patterns. Examples: 'c49f6a29...', 'ad7c7b01...'.
- `customer_zip_code_prefix` (Integer): The first five digits of the customer's postal code, indicating their geographical location and aiding in logistics and distribution analysis. Examples: 4363, 45436.
- `customer_city` (String): The name of the city where the customer resides, providing insights into the urban or rural distribution of the clientele. Examples: 'itupeva', 'sao paulo'.
- `customer_state` (String): The two-letter code representing the state within Brazil where the customer is located, essential for regional sales and marketing strategies. Examples: 'CE', 'SP', 'MG'.

Use Case:
This dataset is invaluable for stakeholders aiming to enhance customer relationship management (CRM) systems, improve targeted marketing strategies, and analyze regional sales trends. By leveraging the unique identifiers, businesses can track customer loyalty and recurring purchasing behaviors. Additionally, geographic attributes enable detailed market segmentation and logistics optimization. Marketers, data analysts, and supply chain specialists will find this dataset particularly useful for crafting personalized offers, identifying potential markets, and efficient resource allocation.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46105) of an [OpenML dataset](https://www.openml.org/d/46105). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46105/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46105/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46105/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

